Reading 09 - Cameron Pickard
============================
1
	1. The size of s is 12 ( 4 + 8).
   	The size of u is 8 (8 > 4).
	2. Structs contain multiple variables, each with their own allocated 
	space in memory. A union contains a single variable that can go by 
	different names and types. The size of a struct is sum of all 
	components, while the size of a union is simply the size of the 
	largest 
	component.
2.  
	1. fate is inexorable!
	2. DUMP_BITS, when displaying t.v0, v1, or v2, shows 64 bits of information. This is because the largest data type in
		each union is 8 bytes (long). So regardless of whether the value for v0, v1 or v2 is stored as an int or a long, internally it will always be
		interpreted as a long, and that will be represented as 64 bits. 
		
		To display the message, the program sets a pointer to a cstring = to the char typecasted address of our t struct, iterates through the struct's 24 bits 
		of memory(8*3), and outputs each bit's char representation. 
		
		If one calls DUMP_BIT on t.v0, t.v1, and t.v2, the ouput would be a set of 24 bits across 3 lines . The char representation of those 24 bits would look 
		as follows:
		
		  si etaf
		 baroxeni
		      !el
		      
		or, "fate is inexorable!"
		
	3. This tells me that, no matter what type we think we are declaring a variable as in C, it is always represented internally as a series of bytes 
	consisted of bits each. The amount of bytes varies by type (for instance a char is a single byte, which is 8 bytes, which is 2 bits in hexadecimal). The 
	conversion of a variable to a readable form (such as int or char) is only done for our personal convenience. The computer always prefers to store the 
	variables as a series of bytes.

3.
	1. A collision occurs when multiple values are attempted to be assigned to the same bucket and the program does not forsee it. 
	2. In separate chaining, the value associated with each bucket is actually a list. When a new value wants to be placed in each bucket, the value is simply 
	appended to the list inside that bucket. If the sum of the number of values in each list exceeds the number of buckets, then the table is doubled, and lists 
	are re-inserted in the new table.
	3. In a linear hash table, each bucket contains a set amount of values (usually 1). This means a different strategy has to be taken when trying to add a 
	value to a bucket that already contains a value. When trying to insert a value into a bucket that already contains a value, the program will instead try to 
	force it into the next bucket until it finds an open bucket (if the final bucket is reached, the "next" bucket to be checked will then be the first bucket 
	in the table). If over, half the buckets contain values or deleted values, the hashtable is enlarged.
	
4. 
	1.    Bucket | Value
		-----------------
			0	 |
			1    |
			2	 | 2 72
			3	 | 3 13
			4	 | 14
			5	 |
			6	 | 56
			7 	 | 7
			8	 | 78 68
			9 	 | 79
			
	2.
		  Bucket | Value
		-----------------
			0	 | 68
			1    | 14
			2	 | 2
			3	 | 3
			4	 | 72
			5	 | 13
			6	 | 56
			7 	 | 7
			8	 | 78 
			9 	 | 79
