Reading 07 - Cameron Pickard
============================
Part 1
1.) The while loop runs MAX_NUMBERS of times (512), while scanf is not 
returning EOF (scanf returns the number of converted items - in this 
case, integer type chars found in the stream are converted into elements 
in an array - or it returns EOF if the end of the stream is reached).

2.) You cannot use sizeof here, because ints take up 4 bytes, so the 
number returned by sizeof would be 4 times bigger than what would be 
intended.

Part 2
1.) In cat.c, the while loop hat parses the command line arguments in 
this only runs while a certain counter is less than argc. cat.py used a 
counter of sorts in its while loop, but it wasn't explicitly a counter. 
Instead, it evaluated the length of argv as values from argv were 
sequentially popped off.

Another difference is how printing works. It is much simpler in cat.py. 
There, a for loop is run for each line in the stream. After the line is 
stripped of white space on the right side using rstrip(), the line + the 
designated ENDING is then printed with a simple print command that 
executes each run through the for loop. In cat.c, the process is far 
more meticululous. A series of if statements are run to see if either 
argind == agc or if the c string representing the next argument is not a 
"-". If either are true, elements from stdin are written to stdout using 
fputs.

2. After the initial flags have been evaluated from earlier, argind has 
a certain value as we enter this while loop. The while loop basically 
runs as long as argind is less than the number of arguments (argc). It 
bascially evaulates the remaining arguments.

It then sets a c string equal to the next of the remaining values of the 
array of arguments. If the "file name" is "-", then a function called 
cat_stream is run that basically prints out the contents of stdin using 
cat_stream. Otherwise, cat_file has to be called to attempt to open the 
file, and if it can, than it will all cat_stream from within.

3. cat_stream takes the stream and reads each line to a variable called 
buffer. This is done using fgets. Then within the while loop, fputs is 
called and prints buffer to stdout.

4. cat_file attempts to read in the file name. fs becomes a stream that 
contains the contents of the file. This is done using fopen. Then, the 
function checks to see if the stream is NULL (if an error occured while 
trying to open the file). If so, an error is printed and the function 
exits. This is done using strerror, which prints the string associated 
with errno, the number of the last error. Otherwise, cat_stream is 
called, the fs stream is passed as an argument, and then the stream is closed 
by calling fclose.
