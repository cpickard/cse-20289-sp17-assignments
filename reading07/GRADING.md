reading07 - Grading
===================

**Score**: 2.75/4

Deductions
----------
-0.50 - test_sort.sh fails
-0.25 - insertion sort is not implemented correctly
-0.50 - test_grep.sh fails

Comments
--------
I could not get your programs to compile, you should test them a little more to
make sure the graders can compile the programs to have a better chance of
getting points.
