/* sum.c */

#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define MAX_NUMBERS (1<<10)

/* Functions */

size_t read_numbers(FILE *stream, int numbers[], size_t n) {
	size_t i = 0;
	
	while (i<n && scanf("%d", &numbers[i]) != EOF) {
		i++;
	}
	
	return i;
}

void sort_numbers(int *x, size_t n) {
	int *z;
	int *y;
	z=x;
	y=z;
	int changes = 1;
	int placeholder;
	while (changes!=0) {
		changes=0;
		for(size_t i = 0; i<n-1; i++) {
			if(i==0) {
				z=x;
			}
			y=z;
			y++;
			if(*z>*y) {
				placeholder=*y;
				*y=*z;
				*z=placeholder;
				changes=1;
			}
			z++;
		}
	}
}
/* Main Execution */

int main(int argc, char *argv[]) {
	int numbers[MAX_NUMBERS];
	size_t nsize;
	
	nsize = read_numbers(stdin, numbers, MAX_NUMBERS);
	sort_numbers(&numbers[0], nsize);
	int *x;
	x = &numbers[0];
	for(size_t i = 0; i<nsize; i++) {
		printf("%d ", *x);
		x++;
	}
	printf("\n");
	return EXIT_SUCCESS;
}
