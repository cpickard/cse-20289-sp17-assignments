Reading 03 - Cameron Pickard
============================

1. $ echo "All your base are belong to us" | tr a-z A-Z

2. $ echo "monkeys love bananas" | sed 's/monkeys/gorillaz/g'

3. $ echo "           monkeys love bananas" | sed -e 's/^[ \t]*//'

4. $ cat /etc/passwd | grep '^root'

5. $  sed -e 's/\/bin\/bash/\/usr\/bin\/python/g' /etc/passwd -e 's/\/bin\/csh/\/usr\/bin\/python/g' /etc/passwd -e 's/\/bin\/tcsh/\/usr\/\usr\/bin\/python/g' 
/etc/passwd

6. $ cat /etc/passwd | grep '4[[:digit]]*7'm

7. comm -12 textfile1.txt textfile2.txt

8. diff file1.txt file2.txt

