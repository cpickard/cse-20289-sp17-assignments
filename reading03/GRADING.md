## GRADING
### Reading03
##### Deductions
-0.25: 5 - Close but you changed too many things; The correct command is: cat /etc/passwd | sed -r 's|/bin/[batc]+sh|/usr/bin/python|' | grep python and you can see that it only changed 5 things.  When your command is piped to wc, you can see that it changed 10 things.
---
##### Final Grade: 3.75/4.0
---
*Graded by Maggie Thomann*
