GRADING.md
==========

Questions
---------

`-.25` 1: bash exists.sh OR sh exists.sh

`-.25` 3: You need to type `./exists.sh` into the command line. the `./` means the following is an executable.

`-.25` 4: I'm not sure you understand the concept of the shell. Unix isn't a piece of hardware, or it's more of a philosophy, or a set on principals on which Operating Systems adhere too. `#!/bin/sh` is the shebang, it tells the shell which interpreter to use when executing the script.

exists.sh
---------

`-.75` Two things wrong with your `exists.sh`. First, you skipped modification 4, where you have to display an error message and exit if no arguments are given. Second, your script doesn't test every command line argument (one at a time), because it quits once you find a file that doesn't exist, and doesn't continue testing the rest of the arguments. To solve this you need to set on `exitcode` variable, have it default to `0`, set it equal to `-1` if it finds a file that doesn't exist, and call `exit $exitcode` at the very end of yur program, outside your loop.


**Total: 2.5/4**