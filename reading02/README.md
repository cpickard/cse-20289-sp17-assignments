Reading 02
==========
1. Type: ./exists.sh   into the command line.

2. Type:  chmod 755 exists.sh    into the command line

3.Type: exists.sh        into the command line

4. This tells Unix the shell is to be executed by /bin/sh, or the Bourne 
shell. This works even if the file is executed in other shells 
such as C++.
   
5. README.md exists!

6. It represents the first parameter after the executable.

7. It tests to see if the parameter after the executable exists.

8. It tests to see if the parameter exists. If not, a message saying 
"$1 does not exist!" will be displayed. 


exists.sh
#!/bin/sh

while [ "$#" -gt "0" ]
do
	if [ -e "$1" ]; then
		echo "$1 exists!"
	else
		echo "$1 does not exist!"
	fi
	shift
done
