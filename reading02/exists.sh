#!/bin/sh

while [ "$#" -gt "0" ]
do
	if [ -e "$1" ]; then
 		echo "$1 exists!"
	else
		echo "$1 does not exist!"
		break
	fi
	shift
done
