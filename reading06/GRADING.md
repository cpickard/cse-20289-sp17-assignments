Reading 06 - Grading
=========================
**Score**: 3.75/4

**Grader**: Mimi Chen


Deductions
----------------
(-0) Computations are not exactly made easier with MapReduce. MapReduce basically just processes large amounts of data using many machines

(-0.25) Shuffling is not the consolidation of output. Shuffling organizes/ forwards data from mappers to appropriate reducers


Comments
----------------

