#!/usr/bin/env python2.7
import sys

print ' '.join([x for x in map(lambda y: y.strip(), sys.stdin) if int(x) % 2 == 0])
