Reading 06 - Cameron Pickard
============================
2.A
	MapReduce simplifies parallel and distributed systems by allowing 
	for more simple computations, the hiding of "messy details" of 
	parralelization, fault-tolerance, data distributionk, and load 
	balancing in a library, and increases the ease with which 
	programmers can utilize the resources of a large distributed system.
2.B
	Mapping - Data from each input split is passed to a mapping 
	function. Each mapping function produces a set of output values.
	
	Shuffling - The outputs of each function are consolidated here. 
	Repeat values are removed.
	
	Reducing - The results of each function are aggregated and 
	summarized data is outputed.

