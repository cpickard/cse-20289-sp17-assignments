GRADING - Exam 02
=================

- Identification:   2.5
- Web Scraping:     3.75
- Generators:       5.75
- Concurrency:      4.5
- Translation:      3.5
- Total:	    20.0
