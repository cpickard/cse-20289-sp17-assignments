GRADING.md
==========

`-.2` 1e: Not quite, you're right in that the order is different, but you needed to say that the standard error of the sort command is supressed, not the du command (as in the first case)

`-.2` 2c, 2e: {1...6} should be {1..6}

`-.2` 4c: ctrl-d, ctrl-c terminates the process and is the answer for 4d

`-.2` 4e: if you're going to use kill, you need the PID. `kill PID` and `pkill bc` are both acceptable answers.

3.2/4