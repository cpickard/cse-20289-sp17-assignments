Reading 01
==========
- Cameron Pickard
1.a
	The "|" is called a pipeline. If you have "a | b", where a and b are
	commands, then the output of a command a is linked to the input of 
	command b.
1. b
	"2> /dev/null" sends any error or status messages, that would normally
	have been displayed, to a bit bucket called /dev/null. It does nothing
	with the information it receives. This command basically disposes of 
	error and display messages.
1.c
	"> output.txt" redirects any standart output to a file called
	output.txt. Standard error is not sent to this file.
1.d
	"-h" makes the output more readable to the user. In this case, it
	specifies what byte size each number is quantifying.
1.e
	It produces the same result but the order in which things are done is
	different. Although the standard error is not disposed
	of before "> output.txt" is read, it doesn't matter, because the ">" 
	command only redirects standard output. In this case, the standard 
	output is redirected, and then the standard error is disposed of. In
	the original command, the standard error is disposed, and then the 
	standard output is redirected.

2.a
	cat 2002* > 2002
2.b	
	cat *12 > December
2.c
	cat -*{01...06}
2.d
	cat {2002,2004,2006}-{01,03,05,07,09,11}
2.e
	cat {2002...2004}-{09-12}

3.a
	The Huxley and Tux files
3.b
	The Huxley and Tux files
3.c
	None of them.
3.d
	chmod 755 Tux
3.e
	chmod 600 Tux

4.a
	kill -19 PID - where PID represents the PID number of the bc process
4.b
	kill -18 PID
4.c
	bc stops reading input when the string "quit" is found. You can also
	terminate the process by hitting ctrl + c
4.d
	kill [-15] PID , or just kill PID  , as 15 is the signal sent by 
	default.
4.e
	kill [- username] [-15] bc
