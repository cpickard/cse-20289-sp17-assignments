/* final.c: simple TCP echo client */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

const char *host = "xavier.h4x0r.space";

FILE *socket_dial(const char *host, const char *port) {
    /* Lookup server address information */
    struct addrinfo *results;
    struct addrinfo  hints = {
        .ai_family   = AF_UNSPEC,   /* Return IPv4 and IPv6 choices */
        .ai_socktype = SOCK_STREAM, /* Use TCP */
    };
    int status;
    if ((status = getaddrinfo(host, port, &hints, &results)) != 0) {
    	fprintf(stderr, "getaddrinfo failed: %s\n", gai_strerror(status));
	return NULL;
    }

    /* For each server entry, allocate socket and try to connect */
    int client_fd = -1;
    for (struct addrinfo *p = results; p != NULL && client_fd < 0; p = p->ai_next) {
	/* Allocate socket */
	if ((client_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
	    fprintf(stderr, "Unable to make socket: %s\n", strerror(errno));
	    continue;
	}

	/* Connect to host */
	if (connect(client_fd, p->ai_addr, p->ai_addrlen) < 0) {
	    fprintf(stderr, "Unable to connect to %s:%s: %s\n", host, port, strerror(errno));
	    close(client_fd);
	    client_fd = -1;
	    continue;
	}
    }

    /* Release allocate address information */
    freeaddrinfo(results);

    if (client_fd < 0) {
    	return NULL;
    }

    /* Open file stream from socket file descriptor */
    FILE *client_file = fdopen(client_fd, "w+");
    if (client_file == NULL) {
        fprintf(stderr, "Unable to fdopen: %s\n", strerror(errno));
        close(client_fd);
        return NULL;
    }

    return client_file;
}

int main(int argc, char *argv[]) {
    /* Connect to remote machin */
    FILE *client_file;
    FILE *fs;
    // Find an open socket
    for(int port = 9700; port < 9800; port++) {
    	char buffer[BUFSIZ];
    	sprintf(buffer, "%d", port);
    	client_file = socket_dial(host, buffer);
    	if(client_file!=NULL) {
    		printf("%d is open\n", port);
    		break;
    	}
    }
    if((fs=fopen("final.c", "r")) == NULL) {
    	perror("Error with fopen: ");
    } 

    /* Read from stdin and send to server */
    char line[BUFSIZ]="PUT cpickard 1024";
    char fileline[BUFSIZ];
    size_t nread;
    fputs(line, client_file);
    fgets(line, BUFSIZ, client_file);
    fputs(line, stdout);
    while((nread = fread(fileline, 1, BUFSIZ-1, fs)) !=0) {
    	fwrite(fileline, nread, 1, client_file);
    }
    
    // Get that PUT line
    
    //while (fgets(line, BUFSIZ, stdin)) {
    //    fputs(line, client_file);
    //    fgets(line, BUFSIZ, client_file);
    //    fputs(line, stdout);
	//}
    // Write from file to socket
    /*char fileline[BUFSIZ];
    size_t nread;
	while((nread = fread(fileline, 1, BUFSIZ-1, fs))!=0) {
		fwrite(fileline, nread, 1, client_file);
	}*/
    fclose(client_file);
    fclose(fs);
    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
