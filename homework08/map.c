/* map.c: separate chaining hash table */

#include "map.h"

/**
 * Create map data structure.
 * @param   capacity        Number of buckets in the hash table.
 * @param   load_factor     Maximum load factor before resizing.
 * @return  Allocated Map structure.
 */
Map *	        map_create(size_t capacity, double load_factor) {
	Map * newmap = calloc(1, sizeof(Map));
	if(newmap) {
		if(load_factor<=0) {
			newmap->load_factor=DEFAULT_LOAD_FACTOR;
		}
		else {
			newmap->load_factor=load_factor;
		}
		if(capacity==0) {
			newmap->capacity=DEFAULT_CAPACITY;
		}
		else{
			newmap->capacity=capacity;
		}
	
		newmap->buckets=calloc(newmap->capacity, sizeof(Entry));
		return newmap;
	}
	return NULL;
}

/**
 * Delete map data structure.
 * @param   m               Map data structure.
 * @return  NULL.
 */
Map *	        map_delete(Map *m) {
	for(int i=0; i< m->capacity; i++) {
		entry_delete((m->buckets[i]).next, true);
	}
	free(m->buckets);
	free(m);
    return NULL;
}

/**
 * Insert or update entry into map data structure.
 * @param   m               Map data structure.
 * @param   key             Entry's key.
 * @param   value           Entry's value.
 * @param   type            Entry's value's type.
 */
void            map_insert(Map *m, const char *key, const Value value, Type type) {
	if(((double)(m->size)/(m->capacity))>(m->load_factor)) {
		map_resize(m, 2*(m->capacity));
	}
	uint64_t i = fnv_hash(key, strlen(key))%m->capacity;
	Entry * curr = &(m->buckets[i]);
	while(curr->next) {
		curr=curr->next;
		if(strcmp(curr->key, key)==0) {
			entry_update(curr, value, type);
			return;
		}
	}
	(m->size)++;
	curr->next=entry_create(key, value, NULL, type);
}

/**
 * Search map data structure by key.
 * @param   m               Map data structur
 * @param   key             Key of the entry to search for.
 * @param   Pointer to the entry with the specified key (or NULL if not found).
 */

Entry *         map_search(Map *m, const char *key) {
    uint64_t i = fnv_hash(key, strlen(key))%(m->capacity);
    Entry *curr = (m->buckets[i].next);
    while(curr) {
    	if(strcmp(curr->key, key)==0) {
    		return curr;
    	}
    	curr=curr->next;
    }
    return NULL;
}

/**
 * Remove entry from map data structure with specified key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to remove.
 * return   Whether or not the removal was successful.
 */
bool            map_remove(Map *m, const char *key) {
	uint64_t i = fnv_hash(key, strlen(key))%m->capacity; // i=bucket
	Entry * curr = &(m->buckets[i]);
	while(curr->next) {
		if(strcmp(curr->next->key, key)==0) {
			Entry * temp=curr->next;
			curr->next=curr->next->next;
			entry_delete(temp, false);
			(m->size)--;
			return true;
		}
		curr=curr->next;
	}
    return false;
}

/**
 * Dump all the entries in the map data structure.
 * @param   m               Map data structure.
 * @param   stream          File stream to write to.
 * @param   mode            Dump mode to use.
 */
void		map_dump(Map *m, FILE *stream, const DumpMode mode) {
	for(int i = 0; i< m->capacity; i++) {
		Entry * curr = (m->buckets[i]).next;
		while(curr) {
			entry_dump(curr, stream, mode);
			curr=curr->next;
		}
	}
}

/**
 * Resize the map data structure.
 * @param   m               Map data structure.
 * @param   new_capacity    New capacity for the map data structure.
 */
void            map_resize(Map *m, size_t new_capacity) {
	Entry * newbuckets=calloc(new_capacity, sizeof(Entry));
	for(int i=0; i< m->capacity; i++) {
		Entry * curr = (m->buckets[i]).next;
		while(curr) {
			uint64_t ind = fnv_hash(curr->key, strlen(curr->key)) % new_capacity;
			Entry * temp=curr->next;
			curr->next=newbuckets[ind].next;
			newbuckets[ind].next=curr;
			curr=temp;
		}
	}
	m->capacity=new_capacity;
	free(m->buckets);
	m->buckets=newbuckets;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
