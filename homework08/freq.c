/* freq.c */

#include "map.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;
/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
    fprintf(stderr, "    -f FORMAT        Output format (KEY, KEY_VALUE, VALUE, VALUE_KEY)\n");
    fprintf(stderr, "    -l LOAD_FACTOR   Load factor for hash table\n");
    exit(status);
}

void freq_stream(FILE *stream, double load_factor, DumpMode mode) {
	Map *m = map_create(0, load_factor);
	char word[BUFSIZ];
	while(fscanf(stream, "%s", word) != EOF) {
			Entry * found = map_search(m, word);
			if(found) {
				(found->value.number)++;
				map_insert(m, word, found->value, NUMBER);
			}
			else {
				Value temp;
				temp.number=1;
				map_insert(m, word, temp, NUMBER);
			}
	}
	map_dump(m, stdout, mode);
	map_delete(m);
}

/* Main Execution */

int main(int argc, char *argv[]) {
	double LOAD = 0;
	DumpMode MODE = VALUE_KEY;
	char * form = NULL;
    /* Parse command line arguments */
	int argind=1;
	PROGRAM_NAME=argv[0];
	while(argind<argc && strlen(argv[argind]) > 1 && argv[argind][0]=='-') {
		char *arg=argv[argind++];
		switch(arg[1]) {
			case 'h':
				usage(0);
				break;
			case 'f':
				form = argv[argind++];
				if(strcmp(form, "VALUE") ==0) {
					MODE=VALUE;
				}
				else if(strcmp(form, "KEY_VALUE")==0) {
					MODE=KEY_VALUE;
				}
				else if(strcmp(form, "KEY")==0) {
					MODE=KEY;
				}
				else {
					MODE=VALUE_KEY;
				}
				break;
			case 'l':
				LOAD = atof(argv[argind++]);
				break;
			default:
				usage(1);
				break;
		}
	}
	freq_stream(stdin, LOAD, MODE);
    /* Compute frequencies of data from stdin */
    
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
