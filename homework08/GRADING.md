homework08 - Grading
===================

**Score**: 14.25/15

Deductions
----------
-0.5 - worst case for insert (space and time) is O(n)
-0.25 - worst case for time complexity for search is O(n)
-0.25 - worst case for time complexity for remove is O(n)

Comments
--------
Good work!
