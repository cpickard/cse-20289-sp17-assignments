#!/bin/bash
printf '|  NITEMS   |  ALPHA    |   TIME    |   SPACE      |\n'
printf '| --------  | ------    | --------  | --------     |\n'
for num in 1 10 100 1000 10000 100000 1000000 10000000; do 
	for load in 0.5 0.75 0.9 1.0 2.0 4.0 8.0 16.0; do
		Value=`shuf -i1-$num -n $num | ./measure ./freq -f VALUE -l $load 2>&1 >/dev/null`
		Time=`echo $Value | cut -d ' ' -f 1`
		Size=`echo $Value | cut -d ' ' -f 3`
		printf '|%-10s | %-9s | %-9s | %-13s|\n' $num $load $Time $Size;
	done
done
