Homework 08 - Cameron Pickard
=============================
Activity 1

1. In entry_create, I allocated memory for an Entry * p by setting to 
malloc(sizeof(Entry)). This allocated memory for a char pointer (key), a  
type value, a pointer to an entry (next), and a union called value. The 
program checks to see if the the type of the value is STRING (according 
to type). If so, space has to be allocated for the string via strdup.

In entry_delete, if the recursive flag is true, a while loop iterates 
through all Entry values in the list by looking at the next values until 
null is found. On each entry, entry_delete with a false value is called. Then 
the original entry (and all entries called with the false passed), have 
their key freed up (since it has specially allocated space), 
e.value.string freed up IF e->type is equal to STRING, and then after 
this, e is freed up.

2. In map_create, space is allocated for newmap (the map pointer that 
will, at the end of the function, be returned) using calloc(1, 
sizeof(Map)). This allocates space for a Map variable and sets all 
internal variables to equal 0. Next, to check to see if allocation 
succeeded, if(newmap) is checked. Everything else is done inside this if 
statement. If the load factor is invalid (aka, anything less than or 
equal to 0 - values that will always be exceeded), newmap is given the 
load factor of DEFAULT_LOAD_FACTOR. Otherwise, the newmap's load factor 
is given the passed load_factor. The same is done for the map's capacity 
(in this case, an invalide capacity is 0). Then, we allocate space for 
the array of buckets within map, by calling calloc, where the number is 
the capacity, and the size of each entry is sizeof(Entry).

In map_delete, we have to deallocate map, the array of buckets, and 
every entry that come after the dummy bucket in the linked lists. 
However, this has to be done in the right order. A for loop is run from 
0 to capacity-1, and entry_delete is called on bucket[i].next with a 
recursive flag of true. This is how the linked list of Entry's in each 
bucket are deallocated. After this for loop, we can delete the array of 
buckets by calling free on the array. This deletes the dummy Entries 
that began the linked lists and the space they took up. After this, we 
can finally deallocate the map simply by calling free on the map.

3. 
   map_resize: This function allocates space for a newbuckets 
   array of 
   Entry values that is double the size of the buckets array contained 
   in m->buckets (2*m->capacity). Then, iterating through every bucket, and within every 
   bucket, iterating through every entry, all entry's keys are rehashed, 
   modded with the new_capacity to determine where the corresponding 
   entry goes in the newbuckets array. Once this index (ind) has been 
   determined, the Entry from the original map that we are currently 
   looking, is pushed to the front of the correct bucket in newbuckets.
   
   After every Entry in the  original bucket has been successfully 
   linked to in newbuckets, m->capacity is set to the new_capacity, 
   m->buckets is freed, and then m->buckets is set to the newbuckets 
   array that was previously allocated in this function. m->buckets is 
   now the double the size of what it was when this function was first 
   called.
   
   map_resize is called whenever map_insert is called and the map's size 
   divided by capacity exceeds the map's load factor.
	
4. map_insert: First, we check to see if the load factor of the map we 
are inserting into has been exceeded. This is done by checking to see if 
m->size dived by m->capacity is greater than m->load_factor. If so, 
map_resize is called on our map (m), and the new capacity is double the 
original.
   
The time complexity for map_insert is O(1), because the average 
number of items in each bucket is load_factor, which means that the 
number of keys in a bucket that we'll have to check (to see if there are 
duplicates) is load_factor. Because load_factor is a constant that never 
changes, and the operations to insert or update an Entry don't increase 
with the number of items, the worst and average case time complexity is 
O(1).

The space complexity for map_insert is also O(1), this is because the 
only data created are two values corresponding to hash table indexes 
(once in insert and once in resize), and a newbuckets array, which is 
equal to the sizeof(Entry) * new_capacity. Becuase the sizeof(Entry) is 
a constant value, and the new_capacity is just double of the old 
capacity (a constant), the overall space complexity - worst and average 
case - is O(1).

5.

map_search rehashes the key from the input and mods with the map 
capacity to find the bucket we are to search in, then we iterate through 
every Entry in the linked list at that bucket. If the Entry's key that 
we're looking at ever equals the key from function argument, that 
Entry's address is returned. If all Entry's have been searched and none 
have had a matching key, then NULL is returned.

The time complexity for map_search is O(1). This is because the average 
number of items in each bucket is load_factor, which means that the 
number of keys in a bucket that we'll have to check (to find an Entry 
with a matching key) is load_factor. Because load_factor is a constant 
that never changes, and the operations to return an Entry's address 
don't increase with the number of items, the worst and average case time 
complexity is O(1).

The space complexity is O(1) - average and worst case.  The only new 
variable being created is the index corresponding to the bucket we want 
to search. This does not change depending on the size of the input 
arguments.

6.
map_remove takes in a Map and string key as arguments. First, the index 
of the bucket we want to search is found by hashing the key's value and 
modding it with m->capacity. Then, an Entry pointer is set to equal the 
address of m->buckets[i] (our index is i). Then a while loop runs while 
(curr->next!=NULL). Inside, curr->next's key is compared to key. If they 
are the same, then curr->next is set to equal curr->next->next (to 
bridge that is about to exist when we delete the Entry), and entry 
delete (false) is called on a temp Entry that was previously set to 
equal curr->next (before it was changed). Finally, m->size is 
decremented, and true is returned, indicating that a match was found and 
removed. 

If all Entry's have been searched and no matches were found, false is 
returned.

The time complexity for map_remove is O(1). This is because the average 
number of items in each bucket is load_factor, which means that the 
number of keys in a bucket that we'll have to check (to find an Entry 
with a key matching what we want to remove) is load_factor. Because 
load_factor is a constant that never changes, and the operations to 
remove an Entry and change the next pointer of the Entry prior to 
the one we removed don't increase with the number of items, 
the worst and average case time complexity is O(1).

The space complexity for map_remove is O(1) -worst and average case. 
This is because only one new variable is instantiated (our index) and 
that doesnt change with size of arguments.

Activity 2
1. As the load factor increased, the time increased but the space 
decreased. This does not surprise me. If the load factor is high, you 
have to resize less often, this means that the space allocated for the 
bucket array will be less for a high load factor because you're resizing 
less often. And it also makes sense that time increases with the load 
factor because with a high load factor, the average amount of Entries in 
a bucket is equal to a bucket. Which means that searching, removing and 
inserting will all take longer with a larger load factor.

2. Hash tables have a less complex method of inserting and removing it's 
components. With a treap this is more complicated becuase you have to 
rearrange a large amount of the treap in worst case scenarios. However, 
treaps are advantagous becuase they only allocate memory for data that 
actually exists. Hash tables allocates memory for an array of buckets, 
but if nothing is ever inserted into one or more of the buckets, that 
memory is wasted. Furthermore, the elements of a treap are already 
sorted, which makes it easier to find the greatest or smallest value.

Personally, I would use a hash table. It seems far less complicated 
conceptually and when it comes to removing and inserting values. I 
expect this would have a big impact on how much time it takes to run the 
program.
