#!/bin/sh

# gender.sh

URL=https://www3.nd.edu/~pbui/teaching/cse.20289.sp17/static/csv/demographics.csv

count_gender() {
    column=$1
    gender=$2
    # TODO extract gender data for specified year and group
    case $column in
    	2013) col=1
    		  ;;
    	2014) col=3
    		  ;;
    	2015) col=5
    		  ;;
    	2016) col=7
    		  ;;
    	2017) col=9
    		  ;;
    	2018) col=11
    		  ;;
    	2019) col=13
    		  ;;
    	*) exit 1
    esac
    curl -s $URL | sed 's/,/ /g' | cut -d' ' -f$col | grep -c $gender
}

for year in $(seq 2013 2019); do
    echo $year $(count_gender $year F) $(count_gender $year M)
done
