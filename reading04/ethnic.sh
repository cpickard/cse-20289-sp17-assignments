#!/bin/sh

# ethnic.sh

URL=https://www3.nd.edu/~pbui/teaching/cse.20289.sp17/static/csv/demographics.csv

count_ethnic() {
    column=$1
    ethnic=$2
    case $column in
    	2013) col=2
    		  ;;
    	2014) col=4
    		  ;;
    	2015) col=6
    		  ;;
    	2016) col=8
    		  ;;
    	2017) col=10
    		  ;;
    	2018) col=12
    		  ;;
    	2019) col=14
    		  ;;
    	*) exit 1
    	   ;;
    esac
    curl -s $URL | sed 's/,/ /g' | cut -d' ' -f$col | grep -c $ethnic
}

for year in $(seq 2013 2019); do
    echo $year $(count_ethnic $year C) \
    	       $(count_ethnic $year O) \
    	       $(count_ethnic $year S) \
    	       $(count_ethnic $year B) \
    	       $(count_ethnic $year N) \
    	       $(count_ethnic $year T) \
    	       $(count_ethnic $year U)
done
