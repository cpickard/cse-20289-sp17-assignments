Please use hulk2.py for grading. Despite it's "so close" tagline, it is 
the only submitted version of hulk that is both final and on-time.

I apologize for the confusion this will likely cause for whoever is 
grading. The full situation is explained at the top of hulk_final.py, a 
code which is identical to hulk2.py except for the time of submission 
and explanation at the top of the file. I am trying to ensure that 
whoever is grading is aware that the final code was submitted before the 
due date.
