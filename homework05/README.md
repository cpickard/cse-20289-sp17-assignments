Homework 05 - Cameron Pickard
=============================
Activity 1 (Hulk)
	permutations takes in a length and an alphabet as arguments. It acts 
	recursively. This is done through implementation of a for loop for 
	each "a" in alphabet. Then, for each b in permutations(length-1, 
	alphabet), the function yields a + b. If length==1, then a for loop 
	for each cha in alphabet is run, yielding cha each time. 

	To find a valid passwords, I ran a list comprehension set where for every word in possible, to yield prefix + word if md5sum(prefix+word) was in hashes).
	
	The time it took to compute passwords decreased as the number of cores decreased.
	Length 6
	1 core ---- >3 and a half hours
	8 cores --  >45 minutes
	
	(Neither finished at time of writing)
	
	A longer password increases the complexity more than a longer alphabet. The complexity increases linearly with a longer alphabet, but exponentially with a 
	longer password.
	
Activity 2 (Map Reduce)
  iv_map.py
	The program keeps track of line numbers by instantiating a counter 
	variable (equal to 0) before the for loop that runs through the 
	lines in sys.stdin(). Then at the beginning of each loop, increments 
	counter by 1.
	
	To remove all punctuation except hyphens, we set a variable called 
	stringWHyphen equal to string.punctuation. Then, we set 
	stringWHyphen = string.WHyphen.replace("-", ""). This removes the 
	hyphen from our customized set of punctuation. Then we format it 
	properly by setting pattern = r"[{}]".format(stringWHyphen). This is 
	applied inside the for loop that checks each "word" in each line, 
	when word is set equal to re.sub(pattern, "", word). What this does 
	is changes characters from pattern that exist in word equal to "". 
	Because of the modificiations implemented earlier, hyphens will 
	remain.
	
  iv_reduce.py
	To aggregate the results:
		First, a for loop that runs through each line in sys.stdin is 
		run. Two variables - a and b - are set equal to line.split(' 
		',1). a will always be a word, and b will be a line number. b 
		has to be stripped of it's new line character. Then, we append a 
		to a list called "k", and b to a list called "v".
		
		Then, for a for loop that runs for each x in k, we set 
		the value of a dictionary (called counts) as counts[x] = []. 
		This establishes that each value in the dict is of type list. 
		This also handles any duplicates in k. 
		
		We then a run a for loop that runs x from 0 until the length of 
		k -1. If v[x] not in counts[k[x]], then we append counts[k[x]] 
		with v[x]. This is done so that words that appear multiple times 
		in one row will not be appended more than once. 
	
	Output:
		Afterwards, the program runs a run for loop for each key and val 
		in counts. x is printed (with a comma), and then for each y in val, 
		we print y(with a comma). After this, a newline is printed 
		before the process for the next key and val in counts.
