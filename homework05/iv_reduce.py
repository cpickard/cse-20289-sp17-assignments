#!/usr/bin/env python2.7
import sys

counts = {}
k=[]
v=[]
for line in sys.stdin:
	a, b = line.split(' ', 1)
	b = b.rstrip()
	k.append(a)
	v.append(b)
	
for x in k:
	counts[x]=[]
	
counter=len(k)

for x in range(0,counter-1):
	if v[x] not in counts[k[x]]:
		counts[k[x]].append(v[x])
	
for x, v in counts.iteritems():
	print x,
	for y in v:
		print y,
	print "\n"
