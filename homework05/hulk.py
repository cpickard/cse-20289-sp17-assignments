#!/usr/bin/env python2.7

import functools
import hashlib
import itertools
import multiprocessing
import os
import string
import sys

# Constants

ALPHABET    = string.ascii_lowercase + string.digits
ARGUMENTS   = sys.argv[1:]
CORES       = 1
HASHES      = 'hashes.txt'
LENGTH      = 1
PREFIX      = ''

# Functions

def usage(exit_code=0):
    print '''Usage: {} [-a alphabet -c CORES -l LENGTH -p PATH -s HASHES]
    -a ALPHABET Alphabet to use in permutations
    -c CORES    CPU Cores to use
    -l LENGTH   Length of permutations
    -p PREFIX   Prefix for all permutations
    -s HASHES   Path of hashes file'''.format(os.path.basename(sys.argv[0]))
    sys.exit(exit_code)

def md5sum(s):
    ''' Generate MD5 digest for given string.

    >>> md5sum('abc')
    '900150983cd24fb0d6963f7d28e17f72'

    >>> md5sum('wake me up inside')
    '223a947ce000ce88e263a4357cca2b4b'
    '''
    # TODO: Implement
    hashstring = hashlib.md5(s).hexdigest()
    return hashstring

def permutations(length, alphabet=ALPHABET):
    ''' Yield all permutations of alphabet of specified length.

    >>> list(permutations(1, 'ab'))
    ['a', 'b']

    >>> list(permutations(2, 'ab'))
    ['aa', 'ab', 'ba', 'bb']

    >>> list(permutations(1))       # doctest: +ELLIPSIS
    ['a', 'b', ..., '9']

    >>> list(permutations(2))       # doctest: +ELLIPSIS
    ['aa', 'ab', ..., '99']
    '''
    # TODO: Implement 
    length=int(length)
    if length==1:
    	for cha in alphabet:
    		yield cha
    else:
    	for a in alphabet:
    		for b in permutations(length-1, alphabet):
    			yield a + b

def smash(hashes, length, alphabet=ALPHABET, prefix=''):
    ''' Return all password permutations of specified length that are in hashes

    >>> smash([md5sum('ab')], 2)
    ['ab']

    >>> smash([md5sum('abc')], 2, prefix='a')
    ['abc']

    >>> smash(map(md5sum, 'abc'), 1, 'abc')
    ['a', 'b', 'c']
    '''
    # TODO: Implement
    possible = permutations(length, alphabet)
    #for word in possible: 
    #	trword = md5sum(prefix + word)
    #	if trword in hashes:
    #		cracked.append(prefix + word)
    cracked=[prefix + word for word in possible if md5sum(prefix + word) in hashes]		

# Main Execution

if __name__ == '__main__':
    # TODO: Parse command line arguments
    while len(ARGUMENTS) and ARGUMENTS[0].startswith('-') and len(ARGUMENTS[0]) > 1:
    	arg = ARGUMENTS.pop(0)
    	if arg == '-a':
    		ALPHABET=ARGUMENTS.pop(0)
    	elif arg == '-c':
    		CORES = ARGUMENTS.pop(0)
    	elif arg == '-l':
    		LENGTH = ARGUMENTS.pop(0)
    	elif arg == '-p':
    		PREFIX = ARGUMENTS.pop(0)
    	elif arg == '-s':
    		HASHES = ARGUMENTS.pop(0)
    	elif arg == '-h':
    		usage(0)
    	else:
    		usage(1)
    		
    hashdict = [line.rstrip('\n') for line in open(HASHES)]
    if LENGTH>1 and CORES>1:
    	pool = multiprocessing.Pool(int(CORES))
    	halflength = int(LENGTH) / 2
    	subsmash = functools.partial(smash, hashdict, int(LENGTH)-halflength, ALPHABET)
    	newhashdict=itertools.chain.from_iterable(pool.imap(subsmash, (PREFIX + item for item in permutations(halflength, ALPHABET)))) 
    	
    else: 
    	newhashdict = smash(hashdict, LENGTH, ALPHABET, PREFIX)
    for val in newhashdict:
    	print val
    pass
    	
# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
