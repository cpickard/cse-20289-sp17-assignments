/* stringutils.c: String Utilities */

#include "stringutils.h"

#include <ctype.h>
#include <string.h>

/**
 * Convert all characters in string to lowercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	string_lowercase(char *s) {
	char *c;
	for (c = s; *c; c++) {
		*c=tolower(*c);
	}
    return s;
}

/**
 * Convert all characters in string to uppercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	string_uppercase(char *s) {
	char *c;
	for (c = s; *c; c++) {
		*c=toupper(*c);
	}
    return s;
}

/**
 * Returns whether or not the 's' string starts with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' starts with 't'
 **/
bool	string_startswith(char *s, char *t) {
	// Determine the length of T
	const char *c;
	//for(c=t; *t; c++);
	//int length = c-t;
	
	// Compare each letter in t to s
	for(c=t; *c; c++) {
		if(*s != *t) {
			return false;
		}
		s++;
		t++;
	}
    return true;
}

/**
 * Returns whether or not the 's' string ends with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' ends with 't'
 **/
bool	string_endswith(char *s, char *t) {
	if(strlen(t)>strlen(s)) {
		return false;
	}
	char *c;
	//char *x;
	//char *z;
	//z=t;
	
	int lens=strlen(s);
	int lent=strlen(t);
	//c=s+lens-lent;
	for(c=s+lens-lent; *c; c++) {
		if(*c!=*t) {
			return false;
		}
		t++;
	}
	//for(c=t; *c; c++) {
	//	t++;
	//}
	//for(x=s; *x; x++) {
	//	s++;
	//}
	//do {
	//	t--;
	//	s--;
	//	if(*s != *t) {
	//		return false;
	//	}
	//} while (t!=z);
    return true;
    
}

/**
 * Removes trailing newline (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	string_chomp(char *s) {
	char * t = s;
	t+=strlen(s)-1;
	while(*t=='\n') {
		*t=0;
		if(t==s) {
			break;
		}
		t--;
	}
    return s;
}

/**
 * Removes whitespace from front and back of string (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	string_strip(char *s) {
	char * t;
	t= s;
	t+=strlen(s)-1;
	char * end=s+strlen(s);
	//remove from the end
	while(isspace(*t)==1) {
		*t='\0';
		if(t==s) {
			break;
		}
		t--;
	}
	
	//remove from the beginning
	t=s;
	while (isspace(*t)==1)  {
		s++;
		if(t==end) {
			break;
		}
		t++;
	}
    return s;
}

/**
 * Reverses a string given the provided from and to pointers.
 * @param   from    Beginning of string
 * @param   to      End of string
 * @return          Pointer to beginning of modified string
 **/
static char * string_reverse_range(char *from, char *to) {
	if(*to==0) {
		to--;
	}
	char * base;
	char * end;
	char temp;
	base=from;
	end=to;
	while(base!=to) {
		temp=*end;
		*end=*base;
		*base=temp;
		end--;
		if(base==to) {
			break;
		}
		base++;
	}
    return from;
}

/**
 * Reverses a string.
 * @param   s       String to reverse
 * @return          Pointer to beginning of modified string
 **/
char *	string_reverse(char *s) {
	char * end;
	char * base;
	char temp;
	end=s;
	end+=strlen(s)-1;
	base=s;
	for(base=s; *base; base++) {
		temp=*end;
		*end=*base;
		*base=temp;
		if(end==base) {
			break;
		}
		end--;
		if(end==base) {
			break;
		}
	}
    return s;
}

/**
 * Reverses all words in a string.
 * @param   s       String with words to reverse
 * @return          Pointer to beginning of modified string
 **/
char *	string_reverse_words(char *s) {
	char *newword=s;
	s = string_reverse(s);
	char * alsoX=NULL;
	for(char *x=s; *x; x++) {
		if(*x==' ') {
			string_reverse_range(newword, x-1);
			newword=x+1;
		}
		alsoX=x;
	}
	//string_reverse_range(newword, alsoX-1);
	return s;
}
/**
 * Replaces all instances of 'from' in 's' with corresponding values in 'to'.
 * @param   s       String to translate
 * @param   from    String with letter to replace
 * @param   to      String with corresponding replacment values
 * @return          Pointer to beginning of modified string
 **/
char *	string_translate(char *s, char *from, char *to) {
	char *x;
	char *from_x;
	char *to_x;
	int minlength;
	if(strlen(from) > strlen(to)) {
		minlength=strlen(to);
	}
	else{
		minlength=strlen(from);
	}
	for(x=s; *x; x++) {
		from_x=from;
		to_x=to;
		for(int i= 0; i<minlength; i++) {
			if(*x==*from_x) {
				*x=*to_x;
			}
			from_x++;
			to_x++;
		}
    }
    return s;
}

/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int	string_to_integer(char *s, int base) {
	char*y;
	int exp=0;
	int sum;
	int adding;
	char test;
	int it;
	if(base==2 || base == 8 || base == 10 || base ==16) {
		for(y=s+strlen(s); y!=s; y) {
			y--;
			test=tolower(*y);
			adding=1;
			switch(test) {
				case '0':
					break;
				case '1':
					for(it=0; it<exp; it++) {
						adding*=base;
					}
					adding*=1;
					sum+=adding;
					break;
				case '2':
					if(base==2) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=2;
						sum+=adding;
					}
					break;
				case '3':
					if(base==2) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=3;
						sum+=adding;
					}
					break;
				case '4':
					if(base==2) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=4;
						sum+=adding;
					}
					break;
				case '5':
					if(base==2) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=5;
						sum+=adding;
					}
					break;
				case '6':
					if(base==2) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=6;
						sum+=adding;
					}
					break;
				case '7':
					if(base==2) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=7;
						sum+=adding;
					}
					break;
				case '8':
					if(base==2  || base == 8) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=8;
						sum+=adding;
					}
					break;
				case '9':
					if(base==2  || base == 8) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=9;
						sum+=adding;
					}
					break;
				case 'a':
					if(base==2  || base == 8 || base == 10) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=10;
						sum+=adding;
					}
					break;
				case 'b':
					if(base==2  || base == 8 || base == 10) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=11;
						sum+=adding;
					}
					break;
				case 'c':
					if(base==2  || base == 8 || base == 10) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=12;
						sum+=adding;
					}
					break;
				case 'd':
					if(base==2  || base == 8 || base == 10) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=13;
						sum+=adding;
					}
					break;
				case 'e':
					if(base==2  || base == 8 || base == 10) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=14;
						sum+=adding;
					}
					break;
				case 'f':
					if(base==2  || base == 8 || base == 10) {
						return 0;
					}
					else {
						for(it=0; it<exp; it++) {
							adding*=base;
						}
						adding*=15;
						sum+=adding;
					}
					break;
				default:
					return 0;
			}
			exp++;
		}
    	return sum;
    }
    else {
    	return 0;
    }
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
 
