Homework 06- Cameron Pickard
=============================
1. string_reverse_words first reverses the string s via a call to 
string_reverse. After that, a pointer called newword is initially set to s. 
While looping through the characters in the (now reversed) s, if at any 
point the char being looked at is a space, string_reverse_range is called 
with the from being newword (the beginning of the last word) and the to 
being char before the space (the end of the last word). Newword is then set 
to the beginning of the next word.

Time complexity is O(n) because there is a for loop but not a nested one. 
The space complexity is O(1) because a constant amount of variables are 
instantiated (3).

Unfortunately, this program is experiencing a segmentation fault that I am 
unable to figure out.

2. string_translte first finds minlength, which is shorter of the two 
lengths of from and to. It then runs through every char in s, and within 
that is another from loop that runs minlength times (incrementing from and 
to by one each time). If the current char in s ever equals *from, then the 
current char will then equal *to.

The time complexity is O(n^2) because there is a nested for loop. The space 
complexity is O(1) because a constant amount of variables are instantiated 
(5).

3. string_to_int converts a string from a given base to decimal, with the 
valid bases being 2, 8, 10 and 16. First, the program instantiates 
sum to equal 0 and checks to see if the 
base is any of those. If not, the function returns 0. If so, a for loop is 
then run from the end of the string (s+strlen(s)) to the beginning of the 
string. Adding is instantiated to equal 1 here. Then a switch/case 
statement is run to check to see if the char being looked at is between 0 
and 9 or a and e. If not, it wont be a valid digit in the base being used. 
If so though, an if statement is run inside each case to check to see if 
the base is one that is incompatiable with the digit (for instance, a char 
can't equal 2 or 3 or anything else with a base of 2). If not, the correcgt 
amount is assigned to adding based on the current value of exp (the 
exponent) and the char being looked at. Then this number is added to sum.

The time complexity is O(n^2) because there can be a nested for loop 
throughout the entireity of the function (the exception is in cases where 
the current char equals '0' or '1').
Space complexity is O(1) because a constant amount space is being 
allocated for variables being created.


4. A static library is one linked to the programs using it during 
compilation. This causes multiple copies of identical programs thruoughout 
all of one's programs. A shared library is one that is dynamically linked to the 
programs using it upon run time. All function are in a one space, and 
programs can call them as needed. This reduces the amount of memory needed 
compared to when a static library is used. For that reason, libstringutil.a 
is larger.


Part 2

1. Command line parsing was done via a for loop that ran from i = 1 to 
i<argc. It would check if the first char in the string being check 
(argv[i]) was equal to -. If so the appropriate flag would be applied. 
Otherwise the string, and the string after that and the argument would be 
assigned as the from and to strings, for translation.

