Homework06 - Grading
====================

**Score**: 8.5/15

Deductions
----------
-.5 Act1 readme: For Q2 and Q3, should find solution with O(n) time complexity and O(1) space complexity
-.25 Act1 readme: For Q4, libstringutils.so is actually slightly larger than libstringutils.a 
-1.5 test_stringutils.py: 3 tests fail: test05_string_strip, test07_string_reverse_words and test09_string_to_integer
-.25 Act2 readme: Q1 not fully answered
-.5 Act2 readme: Q2 no answer
-3.5 test_translate.py: all test fails

Comments
--------
