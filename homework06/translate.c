/* translate.c: string translator */

#include "stringutils.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = NULL;

enum flags {
	STRIP = 1<<1, 
	REVERSE = 2<<1,
	REV_WORDS = 3<<1, 
	LOWER = 4<<1,
	UPPER = 5<<1;
};
/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
    fprintf(stderr, "Post Translation filters:\n\n");
    fprintf(stderr, "   -s     Strip whitespace\n");
    fprintf(stderr, "   -r     Reverse line\n");
    fprintf(stderr, "   -w     Reverse words in line\n");
    fprintf(stderr, "   -l     Convert to lowercase\n");
    fprintf(stderr, "   -u     Convert to uppercase\n");
    exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, int mode) {
    /* TODO */
}

/* Main Execution */

int main(int argc, char *argv[]) {
	enum flags mode;
	char * start;
    /* Parse command line arguments */
    for(int i =1; i<argc; i++) {
    	if(*argv[i]=='-') {
    		start=argv[i] + 1;
    		switch(*start) {
    			case 's':
    				mode |= STRIP;
    				break;
    			case 'r':
    				mode |= REVERSE;
    				break;
    			case 'w':
    				mode |= REV_WORDS;
    				break;
    			case 'l':
    				mode |= LOWER;
    				break;
    			case 'u':
    				mode |= UPPER;
    				break;
    			default:
    				usage(0);
    		}
   		else{
   			char * from = argv[i];
   			i++;
   			char * to  = argv[i];
   		}
   	}

    /* Translate Stream */

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
