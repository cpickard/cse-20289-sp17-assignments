I wanted to write some comments regarding the homework but didn't have 
the time to include them in my README.md because I was trying to get 
everything in on time.

A few comments regarding the homework:
	- I felt that the homework fell more in line with the 
	material we learned during the week than usual. Unlike other 
	homework assignmenets this semester, this one didn't feel like it required 
	as much beyond and above what I learned in class and from the readings. 
	Some concepts in class still felt fairly rushed though (primarily 
	those seen in Monday's lecture on compiling).
	
	- Despite that however, this homework was still extremely 
	time-consuming and difficult compared to assignments for other  
	classes in our schedule (including some that are worth more 
	than 3 credits). I'm not suggesting it be easy. 
	I welcome challenging work and enjoy when it forces me to think. 
	Assignments for this class typically don't feel like a 
	completely fair challenge though. A lot of research is required to 
	understand concepts needed for the assignment that were either 
	slightly glossed over in class (compilation) or barely talked about 
	at all (bitmode masking, something I still don't quite understand). 
	This wouldn't normally be a problem for a class, if not for 
	the fact that assignments like this require many hours of debugging. 
	I routinely work 12-14 hours on assignments for this class 
	(including this one) and still can't finish them.

 	- I don't know how solvable this is at this point, but there are not 
 	enough TA's. Office hours all throughout Thursday (both in LaFun and 
 	the Engineering Library)  were 
 	packed and it could take a long time to get just a single question 
 	answered. The response times by Professor Bui and the TA's on Slack are 
 	incredible, but it simply doesn't compare to being able to speak to 
 	a TA one-on-one about questions you might have regarding new concepts or 
 	a segmentation fault in your program. This isn't possible when one 
 	TA is responsible for helping 10-20 students at a time. 
 	
 	
 This is the class I feel the strongest about. I want to enjoy learning this 
 material but the factors I mentioned above make work for this class 
 feel like less of a rewarding challenge and more of just constant 
 frustration. I want to end this by saying that I appreciate that
 feedback is consistently welcomed in this class, because it makes me 
 feel comfortable voicing concerns that I may not have normally voiced 
 otherwise.
