#!/bin/sh

usage() {
	cat << EOF
Usage: brofiy.h -h < filename
	-d DELIM   Specifies what lines to remove
	-W         Don't strip empty lines.
EOF
	exit $1
}


LineStrip=0

while [ $# -gt 0 ]; do
	case $1 in
		-d) Delim=$2 
			shift
			;;
		-W) LineStrip=1 
			shift
			;;
		*) usage 1 
		   ;;
	esac
	shift
done

if [ $LineStrip = 0 ]; then
	sed "s|[$Delim].*||g;s/[[:blank:]]*$//g;/^\s*$/d" # remove comments
else 
	sed "s|[$Delim].*||g;s/[[:blank:]]*$//g"
fi
