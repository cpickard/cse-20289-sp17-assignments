#!/bin/sh

usage() {
	cat << EOF
Usage: zipcode.sh -h < filename
	-s STATE   Specifies what state to search for (default: Indiana)
	-c CITY    Specifies what city to search for
	-f FORMAT  Specifies what format to print the info in
EOF
	exit $1
}

State="Indiana"
City=0
Format=0

while [ $# -gt 0 ]; do
	case $1 in
		-s) State=$2 
			shift
			;;
		-c) City=$2
			shift
			;;
		-f) Format=$2
			shift
			;;
		-h) usage 1
			exit 0
			;;
		*) usage 1 
			exit 0
		   ;;
	esac
	shift
done

if [ $Format = 0 ]; then
	if [ $City = 0 ]; then
		curl -s http://www.zipcodestogo.com/$State/ | grep -Eo '[0-9]{5}'
	else
		curl -s http://www.zipcodestogo.com/$State/ | grep -Eo '[$City]\/..\/[0-9]{5}' | grep -Eo '[0-9]{5}'
	fi
else
	if [ $City = 0 ]; then
		curl -s http://www.zipcodestogo.com/$State/ | grep -Eo '[0-9]{5}' | paste -sd ", "
	else
		curl -s http://www.zipcodestogo.com/$State/ | grep -Eo '[$City]\/..\/[0-9]{5}' | grep -Eo '[0-9]{5}' | paste -sd ","
	fi
fi


