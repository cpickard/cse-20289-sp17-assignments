GRADING.md
==========

Activity 1
==========

caesar.sh
----------
`-2`: A couple things wrong with this script. First, you should follow the usage function given in the documentation, not create your own functionality. The should read from `stdin` and rotate the the text by a specified rotation. Your usage function is not produced with the `-h` flag, and you don't have a default rotation of 13.

test_caesar.sh
---------------
`-1`: Most of the tests fail because you didn't follow the documented usage. You failed the usage test because your script actually rotates the `-h` flag.

Task 3
------
good!

Activity 2
==========

broify.sh
----------
`-1`: Your `sed` command is invalid. When I run your script I get the following error: `sed: 1: "s|[].*||g;s/[[:blank:]] ...": unterminated substitute pattern`. I cannot grade your script if it doesn't run! You could've used sed as follows:
`sed -e "s|$DELIMITER.*$||" -e "s|\s*$||" | filter` with `filter` removing newlines.

test_broify.sh
---------------
`-1`: all the tests fail


Task 3
------


Activity 3
==========

zipcode.sh
----------
`-1`: A couple things wrong with this script too. First, you cannot do `[ $Format = 0 ]`, it needs to be `-eq` if testing equality. However, even with that fixed your script still won't work. You instead should do something like:
	
	if [ -z "$CITY" ]; then
	    curl -s "$URL/$STATE/" | filter_zipcodes | format
	else
	    curl -s "$URL/$STATE/" | grep -i "/$CITY/" | filter_zipcodes | format
	fi

test_zipcode.sh
---------------
`-1`: fails all tests

Task 3
------




**Total: 8/15**