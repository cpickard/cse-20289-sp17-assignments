Homework 03 - Cameron Pickard
=============================
Activity 1
1. I implemented a series of if, elif, and else statements that checked 
caused the program to operate differently depending on whether $# 
equalled 0, 1, 2 or something else. If 0, a usage message would be 
displayed. If 1, the sole argument would have a cipher made out of it 
(default 10 character shift). If 2, the first argument would be the 
number representing the shift, and the second argument would be the 
string the cipher would be made out of. If something else, an error 
message would be displayed, and the program would end.

2. If $# equalled 1, MyString = $1. If $# = 2, then MyString = $2.

3. I used the following commands.
	UpdString = $(echo $MyString | tr ${lower:0:25} ${lower:$shif:25}${lower:0:$shifend})
	UpdString = $(echo $UpdString | tr ${upper:0:25} ${upper:$shif:25}${upper:0:$shifend})

	where lower is a string 'abcdefghijklmnopqrstuvwxyz'
	      upper is a string 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	      $shif is (the number the user inputted) mod 26 [by default this is 10]
	      $shifend is ($shif-1) [by default this is 9]
4. MyString has all of its lowercase letters sfifted by a set amount, accounting for the transition between a and z. This becomes UpdString. Then UpdString has 
its uppercase letters shifted by a set amount. At the end of the program, UpdString is called with echo

Activity 2
1. I used a while loop that runs through the arguments after the 
executable. Inside here there's a case statement that looks for 
matches for the flag.
2. sed "s|[$Delim].*||g"
3. sed "/^$/d"
4. If a -W flag was spotted, blank lines are retained.
   The argument that comes after the -d flag became the Delim variable
   
Activity 3
1. Similarly to Activity 2, I used a while loop that ran through arguments after the executable. Inside here there's a case statement
that looks for matches for each of the three flags, -h, and default (*). 
2. I used grep with the -E and -o flags to extractly matches for '[0-9]{5}'
3. State, by default, = "Indiana". City by default, = 0. This can be changed with proper use of the flags. To filter by state, I used a grep statement with 
the -e flag that printed a line containing $State, and then pipelined that into a grep statement with the -Eo flags that printed out any matches in those 
lines for the zip codes (reg expression described in answer to 2).
4. By default, Format = 0. This can be changed with the presence of a -f flag. By default, grep will print in the text format correctly. To print in the csv 
format, the final grep statement has to be pipelined into paste -sd ",".
