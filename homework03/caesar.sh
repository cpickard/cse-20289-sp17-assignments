#!/bin/sh

lower=abcdefghijklmnopqrstuvwxyz;
upper=ABCDEFGHIJKLMNOPQRSTUVWXYZ;


UpdString=""
if [ $# = 0 ]; then
	echo "Usage: caesar.sh x string"
	echo "x: a positive integer."
	echo "string: the string the cipher will be made from"
	exit 1
elif [ $# = 1 ]; then
	MyString="$1";
	UpdString=$(echo $MyString | tr ${lower:0:25} ${lower:10:25}${lower:0:9} )
	UpdString=$(echo $UpdString | tr ${upper:0:25} ${upper:10:25}${upper:0:9} )
elif [ $# = 2 ]; then
	case $1 in
		*[0-9])
			MyString=$2
			arg=$1
			shif=$arg%26
			shifend=($shif)-1
			if [ shif = 0 ]; then
				UpdString=MyString
			else
				UpdString=$(echo $MyString | tr ${lower:0:25} ${lower:$shif:25}${lower:0:$shifend} )
		 		UpdString=$(echo $UpdString | tr ${upper:0:25} ${upper:$shif:25}${upper:0:$shifend} )
		 	fi
		 	;;
		 *)
		 	echo "Error: Not a valid flag. Flag must be an integer."
		 	exit 1
		 	;;
	esac
fi
echo $UpdString
