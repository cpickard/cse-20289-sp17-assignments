/* list.c */

#include "list.h"

/* Internal Function Prototypes */

static struct node*	reverse(struct node *curr, struct node *next);
static struct node *	msort(struct node *head, node_compare f);
static void		split(struct node *head, struct node **left, struct node **right);
static struct node *	merge(struct node *left, struct node *right, node_compare f);

/**
 * Create list.
 *
 * This allocates a list that the user must later deallocate.
 * @return  Allocated list structure.
 */
struct list *	list_create() {
	return calloc(1, sizeof(struct node));
}

/**
 * Delete list.
 *
 * This deallocates the given list along with the nodes inside the list.
 * @param   l	    List to deallocate.
 * @return  NULL pointer.
 */
struct list *	list_delete(struct list *l) {
	//istruct node * next;
	if(l->head) {
		node_delete(l->head, true);
	}
	//for(struct node *curr = l->head; curr; curr=next) {
	//	next = curr->next;
	//	node_delete(curr, false);
	//}
	//while(x) {
	//	x=ournode->next;
	//	node_delete(ournode, false);
	//}
	
	free(l);
    return NULL;
}

/**
 * Push front.
 *
 * This adds a new node containing the given string to the front of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void		list_push_front(struct list *l, char *s) {
	l->size++;
	struct node * p = node_create(s, l->head);
	l->head=p;
	if(l->size==1) {
		l->tail=p;
	}
	return;
}

/**
 * Push back.
 *
 * This adds a new node containing the given string to the back of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void		list_push_back(struct list *l, char *s) {
	l->size++;
	struct node * p = node_create(s, NULL);
	if(l->tail) {
		(l->tail)->next=p;
	}
	l->tail=p;
	if(l->size==1) {
		l->head=p;
	}
	return;
}

/**
 * Dump list to stream.
 *
 * This dumps out all the nodes in the list to the given stream.
 * @param   l	    List structure.
 * @param   stream  File stream.
 */
void		list_dump(struct list *l, FILE *stream) {
	for(struct node *curr = l->head; curr; curr=curr->next) {
		node_dump(curr, stream);
	}
	return;
}

/**
 * Convert list to array.
 *
 * This copies the pointers to nodes in the list to a newly allocate array that
 * the user must later deallocate.
 * @param   l	    List structure.
 * @return  Allocate array of pointers to nodes.
 */
struct node **	list_to_array(struct list *l) {
	//struct node * p[l->size];
	struct node ** p = malloc(sizeof(struct node*)*(l->size));
	struct node * curr = l->head;
	for(int a = 0; curr; a++) {
		p[a]=curr;
		curr=curr->next;
	}
	return p;
	//return NULL;
}

/**
 * Sort list using qsort.
 *
 * This sorts the list using the qsort function from the standard C library.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void		list_qsort(struct list *l, node_compare f) {
    if (l->size <= 1) return;
	struct node ** p = list_to_array(l);
	qsort(p, l->size, sizeof(struct node *), f);
	for(int a= 0; a < (l->size)-1; a++) {
		//if(a+1==l->size) {
		//	p[a]->next=NULL;
		//}
		//else{	
		//	p[a]->next=p[a+1];
		//}
		//curr->next=p[a+1];
		//curr=curr->next;
		p[a]->next=p[a+1];
	}
	l->head=p[0];
	l->tail=p[(l->size)-1];
	l->tail->next=NULL;
	free(p);
}

/**
 * Reverse list.
 *
 * This reverses the list.
 * @param   l	    List structure.
 */
void		list_reverse(struct list *l) {
	struct node * temp = NULL;
	reverse(l->head, temp);
	temp=l->tail;
	l->tail=l->head;
	l->head=temp;
}

/**
 * Reverse node.
 *
 * This internal function recursively reverses the node.
 * @param   curr    The current node.
 * @param   prev    The previous node.
 * @return  The new head of the singly-linked list.
 */
struct node*	reverse(struct node *curr, struct node *prev) {
	if(curr->next) {
		reverse(curr->next, curr);
	}
	curr->next=prev;
    return NULL;
}
/**
 * Sort list using merge sort.
 *
 * This sorts the list using a custom implementation of merge sort.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void		list_msort(struct list *l, node_compare f) {
	l->head=msort(l->head, f);
	for(l->tail=l->head; (l->tail)->next; l->tail=(l->tail)->next);
}

/**
 * Performs recursive merge sort.
 *
 * This internal function performs a recursive merge sort on the singly-linked
 * list starting with head.
 * @param   head    The first node in a singly-linked list.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node *	msort(struct node *head, node_compare f) {
	struct node * left = NULL;
	struct node * right = NULL;
	if(head==NULL || head->next==NULL) 
		return head;
	split(head, &left, &right);
	left = msort(left, f);
	right = msort(right, f);
	head = merge(left, right, f);
    return head;
}

/**
 * Splits the list.
 *
 * This internal function splits the singly-linked list starting with head into
 * left and right sublists.
 * @param   head    The first node in a singly-linked list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 */
void		split(struct node *head, struct node **left, struct node **right) {
	struct node * slow =head;
	struct node * fast =head;
	struct node * tail = head;
	
	while(fast!=NULL && fast->next!=NULL) {
		fast=fast->next;
		fast=fast->next;
		tail=slow;
		slow=slow->next;
	}
	tail->next=NULL;
	*left=head;
	*right=slow;
}

/**
 * Merge sublists.
 *
 * This internal function merges the left and right sublists into one ordered
 * list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node *	merge(struct node *left, struct node *right, node_compare f) {
	struct node * temp = NULL;
	struct node * head = NULL;
	if(f(&left, &right)<0) {
		head=left;
		left=left->next;
	}
	else {
		head=right;
		right=right->next;
	}
	struct node *tail = head;
	while(left && right) {
		if(f(&left, &right)<0) {
			temp=left;
			left=left->next;
			//head->next=left;
			//head=head->next;
			//left=left->next;
		}
		else{
			temp=right;
			right=right->next;
		}
			//head->next=right;
			//head=head->next;
			//right=right->next;
		tail->next=temp;
		tail=temp;
	}
	if(!left) {
		tail->next=right;
	}
	else{
		tail->next=left;
	}
    return head;
}
