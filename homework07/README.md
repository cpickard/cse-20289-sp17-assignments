Homework 07 - Cameron Pickard
=============================
Activity 1
1. When allocating space for the node being created, I set a pointer to a node to malloc(sizeof(struct node)), and then allocated 
space for the string in the node by setting the node's string to strdup(string). When 
deleting space taken up by the node, I called free on the node's string (free(node->string)) and then free on 
the node itself (free(node)). To handle the recursive flag, I iterated through the nodes in the linked 
list(starting with the n->next), and called node_delete(nx, false) on each one. Then after this recursive 
iteration I freed the current node's string and the current node.

2. In list_create, I just returned a block of memory created by calloc(1, sizeof(struct node)). In 
list_delete, I freed memory by calling node_delete with a true flag on the head of the list (this will 
recursively delete all memory taken up by nodes in the list). This was only done if l->head did not equal NULL 
(as in, there is a node in the list). After deleting all of the nodes, I simply freed up the list by calling 
free(l).

3. In qsort, the first thing I did was return if l->size was equal to 1 or 0. Because either it's an empty 
list and can't be sorted (0) or it's a list containing only a single node, that is already sorted (1). After 
this is checked, I create p, an array of addresses to pointers pointing to nodes, and calling list_to_array on 
list that was passed (l). Then I sort the list using qsort and passing in the array (p), the amount of 
elements in the array (l->size), the sizeof(struct node *) and finally the f function, which will be used to 
compare nodes. This sorts the array.

After this I instaniate a pointer that points to nodes and called it curr. I set curr = l->head. Then I 
iterated through the array and set each node in the array's next value to the node ahead of it in the array. 
After this iteration, curr is pointing to the final node in the list. l->tail is set to equal curr here, and 
curr->next is set to equal NULL. After this, the space taken up by the temporary array p is freed up by 
calling free(p).

For time complexity, the worst case is always O(n^2) but the average case is O(nlogn). The  amount of variables that are being 
created each time (via the array) increases as the size of the list increases. Creating the array, and changing the pointers of the nodes in the array both 
involve separate for loops that iterate through the lsit (to create the array) or the array (to set the new next pointers for each node). These are not nested 
though, so the combined complexity is O(n). The call to qsort though, which sorts the array using the pivot, has a worse case-complexity of O(n^2) 
and a best case complexity of O(nlogn). This is the worst time complexity of all operations in the function so, by default, these become the time complexities 
of the overall function.

For space complexity, the average and worst case is always O(n). 
A for loop has to iterate through the entire list in listcreate, and never stops iterating until it iterates through the whole 
thing. Then back in qsort, the entire array is iterated through. Both 
the list and array are O(n). But they aren't nested, so the time complexity remains O(n).

4. Reversing the list involves doing two things, basically: swapping l->head and l->tail, and making all of 
the ->next pointers point to the previous node, instead of the next one. To handle that second 
part,list_reverse calls on reverse and passes l->head and NULL. reverse is a function that, through recursion, 
"switches the direction" of the pointers in all nodes in the list. It takes curr and prev as arguments 
(initially passed as l->head and NULL). These are pointers pointing to NULL. First it checks to see if 
curr->next is not null. If so, it calls on reverse (itself) and passes curr->next and curr as the new curr and 
prev. Then outside of this if statement, it sets curr->next to equal prev. Then NULL is returned.

For space complexity, the worst and average case is O(1). This is because a constant amount of variables are instantiated over listreverse and reverse. reverse 
is called recursively, but reverse itself allocates no new memory. listreverse allocated a single node *. Therefore, the space complexity is O(1). It does p

For time complexity, the worst and average case is also O(n). This is because list_reverse itself has a timecomplexity of O(1), because the amount of commands 
run in it does not increase with an increased list size. reverse has a time complexity of O(n) because it recursively calls itself until every node in the list has had 
its next pointer changed. It essentially calls itself a number of times similar to the size of the list. And it never breaks out of this recursion early, so the 
timecomplexity of the list_reverse function is O(1*n), or O(n).

5. list_msort first sets the head of the new list equal to msort(l->head, f). msort is a function that recursively calls on split(passing head, a pointer to a 
node called left - initially NULL, and right, also NULL) and msort on (left, f) and (right, f). This is done until head->next is NULL, in which return is just 
called (this is done before the calls to split and msort). Split takes in the head to the current sublist, as well as two pointers- initially set to NULL during 
the passing - which will be altered within split. "slow" and "fast" 
pointers to nodes are created and both set initially to head. In a while 
loop, as long as fast != NULL and fast->next != NULL, fast increments 
twice (points two nodes over), while slow increments once(one node 
over). After this while loop exits, *left is set to equal head, and 
*right is set to equal slow. slow will always be pointing to the node 
corresponding to the middle of the current sublist.

The merge function works by taking in a pointer to the  head of the left 
sublist as left, and a pointer to the head of the right sublist as 
right, as well as a node compare function, f. If f determines that left 
is less than right, then the head of the merged list is set to point to 
that node, and then the head of the left sublist points to the new head 
of that sublist. The same would be done if right was less than left. 
Nodes from the sublists keep getting inserted into the final merged list 
in this manner until either the head of the left sublist or the head of 
the right sublist is equal to null. After this, all that needs to be 
done is to set the tail->next of the merged list equal to the remaineder 
of the remaining sublist. Then, head is returned. These functions all 
work together through recursvie calls in msort to divide and conquer the 
overall sort.

For time complexity, the worse case and average time complexity is 
O(n*logn). The merge sort can sort of be thought of as a tree. The 
levels of the tree refer to recursive calls, which each are O(n) time. 
But there are O(logn) levels because we're splitting down the middle of 
various lists, so the total time complexity is O(nlogn), always.

For space complexity, the space complexity is constant is O(logn), 
because the number of variables instantiated in each recursive call is 
constant - O(1) - but there are O(logn) calls in merge sort, so the worse and 
average space complexity is O(logn).


Activity 2:

|  NITEMS   | SORT  |   TIME    |   SPACE   |
| --------  | ----- | --------  | --------- |
|1          | Merge | 0.000999  | 0.480469  |
|1          | Quick | 0.000999  | 0.484375  |
|10         | Merge | 0.000999  | 0.484375  |
|10         | Quick | 0.000999  | 0.492188  |
|100        | Merge | 0.000999  | 0.488281  |
|100        | Quick | 0.000999  | 0.492188  |
|1000       | Merge | 0.000999  | 0.542969  |
|1000       | Quick | 0.000999  | 0.621094  |
|10000      | Merge | 0.007997  | 1.097656  |
|10000      | Quick | 0.006998  | 1.281250  |
|100000     | Merge | 0.107983  | 6.589844  |
|100000     | Quick | 0.093985  | 8.160156  |
|1000000    | Merge | 1.884713  | 61.523438 |
|1000000    | Quick | 1.403785  | 76.828125 |
|10000000   | Merge | 25.854069 | 610.835938|
|10000000   | Quick | 18.016260 | 763.472656|

1. Time and space seem to increase exponentially as the size of the list 
increases (albeit faster for Merge than Quick). This is the case for both sorting methods. 
For instance, the time for all but the last size are less than 2 seconds. However, with 
the final size, the time is approximately 20 seconds for both methods.
 
It was surprising that Quick seemed to complete faster than Merge, since 
its time complexity is O(n^2), which is larger than Merge's 
O(nlog(n)). It was also surprising that despite taking more time to 
complete, Merge used less space than Quick.

2.  The theoretical complexity calculated for sorting complexities is only sometimes predicative of the actual amount of time taken and space needed
to successfully complete the sort. For instance, average and worst case space complexity for Merge is O(logn) while for Quick its O(n). The results fall in line 
with these predictions, as the space needed for Merge is always less than the space needed for Quick. In this instance, the theoretical complexity calculated 
was accurate. However, when looking at the time complexity, one would think that Merge's worst case O(nlogn), average case O(nlogn), would cause it to complete 
faster than Quick's worst case O(n^2), average case (nlogn). However, in our results, we see that Quick always finishes faster than Merge. This tells us that 
theoretical complexities should be interpreted as a general and not definite predictor.

This can be explained due to the fact that QuickSort deals with memory allocated via an array in Cashe, whereas MergeSort deals with memory allocated via 
malloc calls on the RAM. The CPU is designed to be able to fetch and write to memory in Cashe faster than it can for memory in RAM.
