/* lsort.c */

#include "list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;
bool quick = false;
bool numeric = false;
/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
    fprintf(stderr, "  -n   Numerical sort\n");
    fprintf(stderr, "  -q   Quick sort\n");
    exit(status);
}

void lsort(FILE *stream, bool numeric, bool quicksort) {
	char line[BUFSIZ];
	struct list * ourlist = list_create();
	while(fgets(line, BUFSIZ, stream)) {
		list_push_back(ourlist, line);
	}
	if(quicksort) {
		if(numeric) {
			list_qsort(ourlist, node_compare_number);
		}
		else {
			list_qsort(ourlist, node_compare_string);
		}
	}
	else{
		if(numeric) {
			list_msort(ourlist, node_compare_number);
		}
		else {
			list_msort(ourlist, node_compare_string);
		}
	}
	for(struct node * curr = ourlist->head; curr; curr=curr->next) {
		printf(curr->string);
	}
	list_delete(ourlist);
}

/* Main Execution */

int main(int argc, char *argv[]) {
	int argind=1;
	PROGRAM_NAME=argv[0];
    /* Parse command line arguments */
	while(argind<argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
		char *arg=argv[argind];
		switch(arg[1]) {
			case 'h':
				usage(0);
				break;
			case 'n':
				numeric=true;
				break;
			case 'q':
				quick=true;
				break;
		}
		argind++;
	}
    /* Sort using list */
	lsort(stdin, numeric, quick);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
