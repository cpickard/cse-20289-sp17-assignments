#!/bin/bash
printf '|  NITEMS   | SORT  |   TIME    |   SPACE   |\n'
printf '| --------  | ----- | --------  | --------- |\n'
for num in 1 10 100 1000 10000 100000 1000000 10000000; do 
	Value=`shuf -i1-$num -n $num | ./measure ./lsort -n 2>&1 > /dev/null`
	Time=`echo $Value | cut -d ' ' -f 1`
	Size=`echo $Value | cut -d ' ' -f 3`
	printf '|% -10s | Merge | %-9s | %-10s|\n' $num $Time $Size;
	Value=`shuf -i1-$num -n $num | ./measure ./lsort -n -q 2>&1 > /dev/null` Time=`echo $Value | cut -d ' ' -f 1`
	Size=`echo $Value | cut -d ' ' -f 3`
	printf '|%-10s | Quick | %-9s | %-10s|\n' $num $Time $Size;
done
