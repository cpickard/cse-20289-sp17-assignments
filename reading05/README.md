Reading 05 - Cameron Pickard
============================
1.1) import sys imports the command line arguments so that sys.argv can 
be used later to reference the arguments that were passed.

1.2) for arg in sys.argv[1:]: initiates a for loop that runs for each 
command line argument from the second one to the end (i.e., all but the 
file executable)

1.3) The comma allows everything displayed by print to be displayed on 
the same line.

2.1) while len(args) : While there exists a command argument.
	 while args[0].startswith('-') : makes sure to run only when a flag 
	 is found. Otherwise, ENDING will remain '', and not '$'
	 shile len(args) > 1 : While there exists more than 1 command line 
	 argument. Makes sure that there exists something after the flag.
	 
2.2) The first code block : If there are no command arguments, append 
args so that there "exists" an argument (this argument being '-').

The second code block is in a for loop that looks at each element (path) 
in args. If path is '-' (which could only happen if the user passed no 
arguments), then stream will access standard input (stdin).
If path is something else, stream will attempt to access whatever file 
name the path should be.

2.3) This line of code assigns the string that represents a line of code 
in the file being streamed to the variable 'line'. This line of code is 
not necessary. The next line of code could simply be
	print line.rstrip() + ENDING
	

