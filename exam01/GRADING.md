GRADING - Exam 01
=================

- Commands:         1.75
- Short Answers:
    - Files:        3
    - Processes:    2.25
    - Pipelines:    2.5
- Debugging:        3
- Code Evaluation:  2.5
- Filters:          2.75
- Scripting:        2.25
- Total:	    20.0
