## GRADING
### Homework01
##### Deductions
-0.5:  Activity 4 - Ques 2: Didn't use uniq
-1.0:  Activity 4 - Ques 4: Didn't grep for bash.  It should be this: ps aux | grep bash | grep -v grep | wc -l
-0.5:  Activity 4 - Ques 2b: Should have used - pkill -KILL TROLL OR killall -KILL TROLL
---
##### Final Grade: 13/15
---
*Graded by Maggie Thomann*
