Homework 01 - Cameron Pickard
===========
Activity 1

1.a) nd_campus only has the right to lookup files in my home directory, but have the 
right to administer, lookup and lock files in my Public folder, and have no rights of 
any sort to files in my Private folder.

	System:authuser has the right to lookup files in my home directory, and read, 
	lookup and lock files in my Public folder. But they have no rights of any sort to 
	files in my Private folder.
	
1.b) What makes the Public directory public is nd_campus' and sysem:authuser's rights 
to read, lookup and lock files. They do not have these rights for files in my Public 
folder.

2.a) I was not able to create the file. I received a message saying, "touch: cannot 
touch '/afs/nd.edu/web/cpickard.txt': Permission denied"

2.b) The ACLs have priority. I couldn't even look up the directory's ACLs using fs 
listacl /afs/nd.edu/web, which means I didn't have the lookup (l) right.

Activity 2
| Command                                        | Elapsed Time |
|------------------------------------------------|--------------|
| cp -a /usr/share/pixmaps/. ~/images/           | 2.623 s      |
| mv images pixmaps                              |  .005 s      |
| mv pixmaps /tmp/cpickard-pixmaps               | 2.256 s      |
| rm -r /tmp/cpickard-pixmaps                    |  .018 s      |


1.) Renaming a directory is a far less strenuous process than copying each byte of data 
present in a directory to a new directory. This is why the two mv commands differ in time. 
The first is only renaming the directory.

2.) The mv operation copies the data and removes the file from the 
original source, while rm only removes the original source.

Activity 3
1.) bc < math.txt
2.) bc math.txt > results.txt
3.) bc math.txt > result.txt 2> /dev/null/
4.) cat math.txt | bc
	It's calling on more Linux commands. This one has to concatenate the 
	contents of the file and then use the pipeline to use that as the 
	input for bc. With I/O redirection, the entire process is one step 
	shorter, because bc uses the contents of math.txt directly as its 
	input.
	
Activity 4
1.) grep /sbin/nologin /etc/passwd | wc -l
	48
	
2.) users | wc -l
    1
    
3.) ls -S /etc | head -n 5
	termcap
	prelink.cache
	services
	ld.so.cache
	Muttrc
	
4.) users | uniq | wc -w
	15

Activity 5

/afs/nd.edu/user15/pbui/pub/bin/TROLL &
The process has a PID of 27120

1.a) kill 27120
	 kill -15 27120
	 kill -TERM 27120

1.b) kill -9 27120
	 kill -KILL 17664  (new PID)
	 ctrl + c

2.a) killall -u cpickard -9 PID 

2.b) sudo reboot

3.) kill -20 PID
    kill -TSTP PID   yield:
    [2]+ Stopped /afs/nd.edu/user15/pbui/pub/bin/TROLL
