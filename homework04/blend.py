#!/usr/bin/env python2.7
# Cameron Pickard
# Homework 4, Part 1
# This program creates a gif that is comprised of different blends of two images.

# NOTE: This program is not able to find the profile picture .jpeg in the source code. Therefore, it does not achieve the desired result. However, the rest of 
# the code is written.
import atexit
import os
import re
import shutil
import sys
import tempfile

import requests
# Global variables

REVERSE     = False
DELAY       = 20
STEPSIZE    = 5

# Functions

def usage(status=0):
    print '''Usage: {} [ -r -d DELAY -s STEPSIZE ] netid1 netid2 target
    -r          Blend forward and backward
    -d DELAY    GIF delay between frames (default: {})
    -s STEPSIZE Blending percentage increment (default: {})'''.format(
        os.path.basename(sys.argv[0]), DELAY, STEPSIZE
    )
    sys.exit(status)

#def search_portrait(netid):
#def download_file(url, path):
#def run_command(command):
# Parse command line options

args = sys.argv[1:]

# TODO: Parse command line arguments
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-r':
		REVERSE = True
    elif arg == '-d':
    	DELAY = args.pop(0)
    elif arg == '-s':
    	STEPSIZE = args.pop(0)
    elif arg == '-h':
    	usage(0)
    else:
    	usage(1)
   
if len(args) != 3:
    usage(1)

netid1 = args[0]
netid2 = args[1]
target = args[2]

# Main execution


# TODO: Create workspace
DIR = tempfile.mkdtemp()

# TODO: Register cleanup
atexit.register(shutil.rmtree(DIR))

# TODO: Extract portrait URLs
Page1 = requests.get('https://engineering.nd.edu/profiles/(%s)' % netid1) #Get source code from webpage
if Page1.status_code != 200:
	print "Error"
	usage(1)
Page2 = requests.get('https://engineering.nd.edu/profiles/(%s)' % netid2)
if Page2.status_code != 200:
	print "Error"
	usage(1)
             #print Page1.text
PicURL1 = re.findall("https://engineering.nd.edu/profiles/.*/@@images", Page1.text) # find a match for the profile.jpeg NOTE: This fails, do not know why
if PicURL1.status_code != 200:
	print "Error"
	usage(1)
PicURL2 = re.findall("https://engineering.nd.edu/profiles/.*", Page2.text)
if PicURL2.status_code != 200:
	print "Error"
	usage(1)

# TODO: Download portraits
A_PIC = requests.get(PicURL1)
n=os.path.basename(PicURL1)
with open(n, 'wb') as fs:
	fs.write(A_PIC.content)
B_PIC = requests.get(PicURL2)
n=os.path.basename(PicURL2)
with open(n, 'wb') as fs:
	fs.write(B_PIC.content)
# TODO: Generate blended composite images
step=0
imgno=0
while step<=100: # increments step by STEPSIZE until step exceeds 100
	step=str(step)
	imgno=str(imgno)
	os.system("composite -blend 'step' A_PIC B_PIC 'DIR'/'imgno'blend.gif") # Create file
	imgno=int(imgno)
	imgno=imgno+1 # Increment. Differentiates the gifs
	step=int(step)
	step=step+STEPSIZE
# TODO: Generate final animation
filestring= " "
imgno=imgno-1
DELAY=str(DELAY)
for i in range(imgno)
	imgno=str(imgno)
	filestring=filestring+ "'DIR'/'imgno'blend.gif "
	imgno=int(imgno)
if REVERSE == False:
	os.system("convert -loop 0 -delay 'DELAY' 'filestring' 'target'")
else:
	for i in range(imgno, 0)
		imgno=str(imgno)
		filestring=filestring+ "'DIR'/'imgno'blend.gif "
		imgno=int(imgno)
	os.system("convert -loop 0 -delay 'DELAY' 'filestring' 'target'")

	
