Homework 04 - Cameron Pickard
=============================
Activity 1
1. I used a series of if and elif statements to evaluate argv[0]. 
Assuming we're in the while loop, argv[0] begins with a '-' character. 
If argv[0] matches up with a flag, a set the proper variable (if there 
is one) to equal argv[0] using pop.

2. I created a temporary directory using tempfile.mkdtemp. All images needed to create the final gif were created in DIR, 
and called later using DIR/$filename.

3. First, I extracted the source code for both of the profiles identified by the user using requests.get. Then, I used 
re.findall with a regular expression to extract the URL for the profile picture.

4. Once I had the urls for the profile pictures, I used requests.get to assign the source code of the pictures to a 
variable. I then called os.path.basename on the same url, followed by an fs.write command. I repeated this for the other 
profile picture.

5. To create the blended images, I created two new variables: step and imgno. Both are initially assigned to 0. I then 
enter a while loop that continues as long as step is less than or equal to 0. Inside, step is changed to a string so it 
can be used in a os.system statement that creates each blend. The file name of the picture is differentiated by imgno, 
which incremented every run through the while loop. Afterwards, both are changed back to ints, and incremented by 1 
(imgno) or by STEPSIZE(step).

6. In order to be able to pass all of the file names of the created images in one string, I created a blank string, and 
ran a for loop from 0 to imgno that added on to it the name of the next file. If REVERSE == True (if 
the user ran the executable with a -r flag), then another for loop is run, this time from imgno to 0, using the same 
process of adding on to the string. After all of this, the os.system("convert") command is run with the proper arguments, 
including our string that we've slowly been increasing.

7. If any sort of error were to cause the program to exit early, the temporary directory would be removed. The program is designed to quit early if there
are an improper amount of command line arguments, or if the status codes of any of the obtained pages do not equal 200.

Activity 2
1. I used a series of if and elif statements to evaluate the command line arguments. 
Inside the while loop (which only runs if there are a certain amount of arguments and the first one begins with a '-'), arg equals argv.pop(0)there are a series 
of if and elif statements that try to find a correct flag. If so, they set the proper variable to argv[0] using pop.

2. I fetched the json data by appending the end of the url in my requests.get function with '.json', and later, instead of reading in r.text, I read in 
r.json(). I noticed that all of the articles were located within 'data', so I used an enumerated for loop (needed due to the fact that the user can limit the 
number of outputs) to searched over everything in r['data'], and within that, assuming our enumerated index was within LIMIT, the program displays the proper 
keys.

3. I filtered by FIELD by doing ti = r['data'][FIELD]. I did not have time to implement  a way to filter based on regular expressions.

4. I did not generate the shortened URL, as it was no longer required.


Other Comments:
-Although my blend.py was unable to successfully find the .jpeg in the profile source code, I greatly appreciated you giving us a fragment of code to 
build off of as well as several hints. 
-However, I still feel as if there is a significant jump in difficulty in the homework assignments compared to what is done for reading assignments and in 
Monday and Wednesday lectures.
- Neither of my codes were able to work correctly due to a similar issue in retrieving either source code or strings within the source code. I assume graders 
check the code after testing the executable, but I still would like to note that the rest of the code was attempted (if that were to go unnoticed for one reason 
or another).


