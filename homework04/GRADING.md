homework04 - Grading
====================

**Score**: 6 / 15

Deductions
----------
-2.75 did not pass any of the tests
-1 doesnt search for image portrait propely
-1 doesnt download image portrait properly
-1 doesnt generate images properly
-1 doesnt generate animation properly

-1.25 didnt pass any tests
-.25 doesnt exit on invalid field
-.25 doesnt fetch json properly
-.25 doesnt iterate over json data properly
-.25  doesnt use re.search to check if regex is in field
Comments
--------
If you notice that you are struggling with the difficulty of assignments and finding what you are supposed to do on your own, you should really try and come to either Dr. Bui's office or a TA office hours, we are all there to help you guys.  

If you feel like I marked off part of the code that was actually correct, please let me know.  I tried substituting in code that i knew worked but because you didnt actually have the results from request there were some things that you could not anticipate and therefore handle correctly.  
