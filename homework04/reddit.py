#!/usr/bin/env python2.7
# Cameron Pickard
# Homework 4, Part 2

import atexit
import os
import re
import shutil
import sys
import tempfile
import requests

# Global Variables
FIELD = "title"
LIMIT = 10
SUBREDDIT = "linux"

headers = {'user-agent': 'reddit-{}'.format(os.environ['USER'])}

def usage(status=0):
	print '''Usage: reddit.py [ -f FIELD -s SUBREDDIT ] regex
		-f FIELD	Which field to search (default: title)
		-n LIMIT	Limit number of articles to report (default: 10)
		-s SUBREDDIT Which subreddit to search (default: linux)'''
	sys.exit(status)
		
args = sys.argv[1:]
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
	arg = args.pop(0)
	if arg == '-f':
		FIELD = args.pop(0)
	elif arg == '-n':
		LIMIT = args.pop(0)
	elif arg == '-s':
		SUBREDDIT = args.pop(0)
	elif arg == '-h':
		usage(0)
	else:
		usage(1)
		
if len(args) >1:
	usage(1)
elif len(args)==1:
	regex=args.pop(0)

	
xr = requests.get('https://www.reddit.com/r/(%s)/.json' % SUBREDDIT, headers = headers)
print xr
if xr.status_code != 200:
	print "Error"
	usage(1)

r = xr.json()

LIMIT = int(LIMIT)
LIMIT=LIMIT-1 # Because index begins at 0
for index, item in enumerate(r['data']):
	if index<=LIMIT:
		ti= r['data'][FIELD]
		au=r['data']['author']
		rl=r['data']['url']
		try:
			print ti
			print "Author: ", au
			print "URL: ", rl
		except KeyError:
			sys.exit(1)
		
