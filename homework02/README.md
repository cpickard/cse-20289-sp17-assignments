Homework 02 - Cameron Pickard
=============================
Activity 1
1.) I used an if statement that checked if $# (an integer representing 
the number of arguments) equaled 0. If so, a message displaying the 
usage of the executable is displayed.

2.) I implemented a case statement.  For *.zip and *.jar files, "unzip 
"$1"" was used. For the other extensions you told us to support, "tar xf "$1" " was used.

3.) Getting the format of this type of file down was the most difficult 
part. I had to frequently reference the Shell Scripting Tutorial, old 
examples I had written for practice, and slack to figure it all out, 
because this coding language is still foreign to me. It can be hard to 
remember all of the proper notation.

Activity 2
1.)
shuf << EOF | head -n 1 | /afs/nd.edu.../cowsay
Line
Line
EOF

What this basically does is shuffle the lines of the here document, pipeline that to head -n 1, which chooses the first line, and then I had that pipelined
to cowsay, which shows a cow displaying the randomly selected message.

2.) I followed the format seen in the shell-scripting tutorial. 
trap cleanup 1 2 15 

cleanup()
{	function
	exit 1
}

Basically when any of those three signals are received, the cleanup function runs. For my error message I used cowsay to display it, and exit 1 ends the 
exectuable.

3.)
	
I used the read function, and assigned the user input to a variable. This was all inside a while loop that wouldn't terminate until the user input did not equal 
"" (aka, nothing).

4.) Figuring out how to pipeline the contents of the here document was challenging. Initially I was placing the pipelining after the final EOF, which I should 
have been placing it after the first. I had to do some research to figure this out.

Activity 3: Meeting with Oracle

1. My first step was to scan 'xavier.h4x0r.space' for open ports:
	$ nc -z xavier.h4x0r.space 9000-9999
	Connection to xavier.h4x0r.space 9097 port [ tcp/*] succeeded!
	Connection to xavier.h4x0r.space 9111 port [ tcp/*] succeeded!
	Connection to xavier.h4x0r.space 9876 port [ tcp/*] succeeded!

2. Then I used netstat pipelined into grep to find an ip address
	$ netstat | grep 9111
	tcp	0	0	student00.cse.nd.edu:57001 129.74.160.130:9111
	#Other stuff. Not relevant
	
3. I attempted to connect to the ip address
	$ wget http://129.74.160.130/
	Connecting to 129.74.160.130:80... failed: No route to host
	
4. I then attempted the same command, but modified with the http port
	$ wget http://129.74.160.130/
	Connecting to 129.74.160.130:9876... connected
	HTTP request sent, awaiting response... 200 NO headers, assuming 
	HTTP/0.9
	Length: unspecified
	Saving to: aindex.html
	#other stuff
	2017-02-02 22:20:36 (588 KB/s) - aindex.htmla
	
5. I then opened the index.html file associated with the non-Doorman of Bobbit message
	$ cat index.html
	"Halt! Who goes there? ....
	....
	Good luck!"
	
6. using
	$ find ~pbui/pub/oracle/lockboxes -name 'cpickard.lockbox'
	I found the location of the lockbox.
	
That's as far as I could get.
	
