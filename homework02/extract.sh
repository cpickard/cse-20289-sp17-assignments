#!/bin/sh


if [ $# = 0 ]; then
	echo "Usage: extract.sh archive1 archive2..."
else
	while [ "$#" -gt "0" ]
	do
		case "$1" in
			*.tgz)
				tar xf "$1"
				;;
			*.tar*)
				tar xf "$1"
				;;
			*.tbz)
				tar xf "$1"
				;;
			*.txz)
				tar xf "$1"
				;;
			*.zip)
				unzip "$1"
				;;
			*.jar)
				unzip "$1"
				;;
			*)
				echo "Not a supported extension."
				;;
		esac
	shift
	done
fi
