#!/bin/sh
trap cleanup 1 2 15

cleanup()
{
	/afs/nd.edu/user15/pbui/pub/bin/cowsay "Have I milked you of all curiosity?"
	exit 1
}

shuf << EOF | head -n 1 | /afs/nd.edu/user15/pbui/pub/bin/cowsay
Have a question?
I know all. Entertain me with a question!
Your lack of knowledge is maddening. Ask something!
Mortal, quiz me!
I shall tell you what you desire to know.
I anticipated your arrival. Ask away.
Let me pour my knowledge into you!
Milk me of my knowledge!
I am a devine cow living in your computer. AMA!
EOF

MYQuestion=""

while [ "$MYQuestion" = "" ]
do
	echo Question:
	read MYQuestion
done

shuf << END_File | head -n 1 | /afs/nd.edu/user15/pbui/pub/bin/cowsay
It is certain
It is decidedly so
Without a doubt
Yes, definitely
You may rely on it
As I see it, yes
Most likely
Outlook good
Yes
Signs point to yes
Reply hazy try again
Ask again later
Better not tell you now
Cannot predict now
Concentrate and ask again
Don't count on it
My reply is no
My sources say no
Outlook not so good
Very doubtful
END_File
