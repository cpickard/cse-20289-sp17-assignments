#include<stdio.h>
#include<dirent.h>
#include<stdlib.h>
#include<sys/stat.h>
int main() {
	struct dirent *dp;
	DIR *dirp = opendir(".");
	if(dirp!=NULL) {
		while((dp=readdir(dirp))!=NULL) {
			//File *file = fopen((const char *)dp, "r");
			struct stat s;
			stat(dp->d_name, &s);
			printf("%s %d\n", dp->d_name, (int)s.st_size);
		}
		closedir(dirp);
	}
	
	return 0;
}
