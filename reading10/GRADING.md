Reading 10 - Grading
========================
**Score**: 3.5/4

**Grader**: Mimi Chen


Deductions
------------------
(-0.25) Q1.3 Use lseek 

		lseek(fd, 10, SEEK_SET);

(-0.25) Q1.4 Use stat and S_ISDIR

		struct stat s;
		lstat(path, &s);		// stat is OK too
		S_ISDIR(s.st_mode);


Comments
------------------
