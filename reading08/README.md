Reading 08 - Cameron Pickard
============================
1.
	1. 1 * 4 = 4 bytes
	2. 5 * 4 = 20 bytes
	3. 4 * 1 + 1 (NULL) = 5 bytes
	4. 2* 4 = 8 bytes
	5. NULL = 1, = 1
	6. 8 bytes (all pointers are 8 bytes)
	7. 8 bytes
	8. 8 bytes
	
2. 
	1. Not enough space was allocated. n*sizeof(int) should have been 
	alocated, not n.
	2. I didn't encounter any errors during my make test.
	

Professor Bui's use of props in both his lecture today on memory 
allocation and Wednesday on pointers before delving into on-screen 
examples has helped me understand the 
concepts more than when he only does stuff on screen for the whole 
lecture. 
