Reading 08 - Grading
========================
**Score**: 1.75/4

**Grader**: Mimi Chen


Deductions
------------------
(-0.25) Q1.3 13 bytes

		Pointer is 8 bytes + 5 chars (need to include NUL)

(-0.25) Q1.4 16 bytes

		Double is 8 bytes, so 2 doubles in point struct

(-0.25) Q1.5 8 bytes

		Pointer is 8 bytes

(-0.25) Q1.6 24 bytes

		Pointer is 8 bytes, and one point struct is 16 bytes

(-0.25) Q1.7 168 bytes

		Pointer is 8 bytes, and 10 point structs

(-0.25) Q1.8 88 bytes

		Pointer is 8 bytes, and 10 pointers to structs

(-0.25) Q2.2 Memory was not freed when there were duplicates

(-0.25) duplicates.c: incorrect invocation of contains function

		if (contains(&randoms[i] + 1, n - i - 1, randoms[i]))

(-0.25) duplicates.c: incorrect memory leak fix. Need to free before return true in line 35

Comments
------------------
